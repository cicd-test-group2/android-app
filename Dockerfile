FROM openjdk:11-jdk-slim
# Switching user can confuse Docker's idea of $HOME, so we set it explicitly
ENV HOME /home/circleci
ARG cmdline_tools=https://dl.google.com/android/repository/commandlinetools-linux-6609375_latest.zip
ARG android_home=/opt/android/sdk
ENV PATH=/opt/flutter/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# SHA-256 92ffee5a1d98d856634e8b71132e8a95d96c83a63fde1099be3d86df3106def9

# Install Ruby
RUN apt-get --quiet update --yes > /dev/null
RUN apt-get --quiet install --yes wget apt-utils tar unzip lib32stdc++6 lib32z1 build-essential ruby ruby-dev > /dev/null
# We use this for xxd hex->binary
#RUN apt-get --quiet install --yes vim-commonflutte
RUN apt-get install --yes git > /dev/null
RUN git --version

# Download and install Android Commandline Tools
RUN  mkdir -p ${android_home}/cmdline-tools && \
    wget -O /tmp/cmdline-tools.zip -t 5 "${cmdline_tools}" && \
    unzip -q /tmp/cmdline-tools.zip -d ${android_home}/cmdline-tools && \
    rm /tmp/cmdline-tools.zip

# Set environmental variables
# deprecated upstream, should be removed in next-gen image
ENV ANDROID_HOME ${android_home}
ENV ANDROID_SDK_ROOT ${android_home}
ENV ADB_INSTALL_TIMEOUT 120
ENV PATH=${ANDROID_SDK_ROOT}/emulator:${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin:${ANDROID_SDK_ROOT}/tools:${ANDROID_SDK_ROOT}/tools/bin:${ANDROID_SDK_ROOT}/platform-tools:${PATH}

RUN mkdir -p ~/.android && echo '### User Sources for Android SDK Manager' > ~/.android/repositories.cfg

RUN yes | sdkmanager --licenses && yes | sdkmanager --update

# Update SDK manager and install system image, platform and build tools
RUN sdkmanager \
  "tools" \
  "platform-tools" \
  "emulator"

RUN sdkmanager \
  "build-tools;27.0.0" \
  "build-tools;27.0.1" \
  "build-tools;27.0.2" \
  "build-tools;27.0.3" \
  # 28.0.0 is failing to download from Google for some reason
  #"build-tools;28.0.0" \
  "build-tools;28.0.1" \
  "build-tools;28.0.2" \
  "build-tools;28.0.3" \
  "build-tools;29.0.0" \
  "build-tools;29.0.1" \
  "build-tools;29.0.2" \
  "build-tools;29.0.3" \
  "build-tools;30.0.0" \
  "build-tools;30.0.1" \
  "build-tools;30.0.2"
RUN yes | sdkmanager --licenses
# install FastLane
COPY Gemfile.lock .
COPY Gemfile .
RUN gem install bundler -v 1.16.6
RUN gem install fastlane-plugin-firebase_app_distribution
RUN gem install fastlane-plugin-google_chat -v 0.1.1
RUN bundle install
RUN bundle update fastlane
RUN git clone https://gitlab+deploy-token-499683:xaN1AQYzvN1sh-oviQM1@gitlab.com/cicd-test-group2/android-app.git

# install flutter sdk
RUN apt install curl --yes
RUN curl -L https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_2.2.3-stable.tar.xz | tar -C /opt -xJ
RUN flutter --version --verbose
RUN flutter channel stable
RUN flutter upgrade
#RUN export PATH=~/flutter/bin:$PATH
RUN flutter doctor --android-licenses
RUN which flutter
RUN which flutter dart
#RUN gem update
#RUN bundle install
#RUN bundle update fastlane
